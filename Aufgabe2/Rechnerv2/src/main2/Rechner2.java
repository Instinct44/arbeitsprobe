package main2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Rechner2 {

	public static void main(String[] args) throws IOException {
		
		//BufferedReader zur Inputverwaltung
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		//Bool-Abfragen zur Schleifenverwaltung der einzelnen Vorgansschritte
		boolean ersteZahlErmittelt = false;
		boolean zweiteZahlErmittelt = false;
		boolean zeichenErmittelt = false;
		
		//BigDecimals, die den Input und das Ergebnis speichern und Strings zur Anzeigeanpassung
		BigDecimal num1 = new BigDecimal("0");
		BigDecimal num2 = new BigDecimal("0");
		BigDecimal ergebnis = new BigDecimal("0");
		
		String ergebnisString = "";
		String sign2 = "";
		
		//Schleife zur Ermittlung der ersten Zahl
		while(!ersteZahlErmittelt){
			System.out.println("Erste Zahl: ");
			String number1 = reader.readLine();
			
			if(!number1.equals("")){
				
				if(number1.contains(",")){
					number1 = number1.replace(",", ".");	
				}
				
				boolean isNegative = false;
				if(number1.contains("-")){
					if(!number1.substring(1, number1.length()).contains("-")){
						isNegative = true;
						number1 = number1.replace("-", "");
					}
					
				}
				
				if (number1.matches("\\d*\\.?\\d+")) {
					if(isNegative){
						number1 = "-" + number1;
					}
					num1 = new BigDecimal(number1);
					ersteZahlErmittelt = true;
					
				}else{
					System.out.println("Es sind nur Zahlen und ein Dezimalpunkt erlaubt. Bitte probieren Sie es noch einmal. ");
				}
				
			}else{
				System.out.println("Leere Eingaben sind nicht erlaubt. Bitte probieren Sie es noch einmal. ");
			}
		}
		
		//Schleife zur Ermittlung des Rechenzeichens
		while(!zeichenErmittelt){
			System.out.println("Rechenoperant (+, -, *, /): ");
			String sign = reader.readLine();
			
			if(!sign.equals("")){
				
				if(sign.toCharArray().length == 1){
					
					if (sign.equals("+")) {
						sign2 = "+";
						zeichenErmittelt = true;
					}else if(sign.equals("-")){
						sign2 = "-";
						zeichenErmittelt = true;
					}else if(sign.equals("*")){
						sign2 = "*";
						zeichenErmittelt = true;
					}else if(sign.equals("/")){
						sign2 = "/";
						zeichenErmittelt = true;
					}else{
						System.out.println("Es sind nur g�ltige Operationszeichen erlaubt. Bitte probieren Sie es noch einmal. ");
					}
					
				}else{
					System.out.println("Es ist nur ein Zeichen erlaubt. Bitte probieren Sie es noch einmal. ");
				}

			}else{
				System.out.println("Leere Eingaben sind nicht erlaubt. Bitte probieren Sie es noch einmal. ");
			}
		}
		
		//Schleife zur Ermittlung der zweiten Zahl
		while(!zweiteZahlErmittelt){
			System.out.println("Zweite Zahl: ");
			String number2 = reader.readLine();
			
			if(!number2.equals("")){
				
				number2 = number2.replace(",", ".");	
				
				boolean isNegative = false;
				if(number2.contains("-")){
					if(!number2.substring(1, number2.length()).contains("-")){
						isNegative = true;
						number2 = number2.replace("-", "");
					}
				}
				
				if (number2.matches("\\d*\\.?\\d+")) {
					
					if(isNegative){
						number2 = "-" + number2;
					}
					num2 = new BigDecimal(number2);
					if(sign2.equals("/") && num2.equals(new BigDecimal(0))){
						System.out.println("Die Division mit 0 ist nicht m�glich. Bitte probieren Sie es noch einmal.");
					}else{
						zweiteZahlErmittelt = true;
					}
					
				}else{
					System.out.println("Es sind nur Zahlen und ein Dezimalpunkt erlaubt. Bitte probieren Sie es noch einmal. ");
				}
				
			}else{
				System.out.println("Leere Eingaben sind nicht erlaubt. Bitte probieren Sie es noch einmal. ");
			}
		}
		
		//Ermittlung des Ergebnisses
		if (sign2.equals("+")) {
			ergebnis = num1.add(num2);
			zeichenErmittelt = true;
		}else if(sign2.equals("-")){
			ergebnis = num1.subtract(num2);
			zeichenErmittelt = true;
		}else if(sign2.equals("*")){
			ergebnis = num1.multiply(num2);
			zeichenErmittelt = true;
		}else if(sign2.equals("/")){
			ergebnis = num1.divide(num2, 50, RoundingMode.HALF_UP);
			zeichenErmittelt = true;
		}else{
			System.out.println("Es sind nur g�ltige Zeichen erlaubt. Bitte probieren sie es noch einmal. ");
		}
		
		//Ergebnisausgabe in die Konsole
		ergebnisString = "" + ergebnis;
		
		System.out.println("Ergebnis: " + ergebnisString);
		
	}

}
