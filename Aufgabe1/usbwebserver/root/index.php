<html>
<head>
	<title>Aufgabe 1</title>
	
	<style>
		body{
			font-family: sans-serif;
			font-size: 1em;
		}
		table{
			width: 60%;
		}
		
		table, th, td{
			border: 1px solid black;
			border-collapse: collapse;
			opacity: 0.95
		}
		
		th, td{
			padding: 1px;
			text-align: center;
		}
		
		th{
			background-color: #ffaaaa;
			color: black
		}
		
		tr:nth-child(even){
			background-color: #dddddd
		}
		
		tr:nth-child(odd){
			background-color: #f8f8f8;
		}
		
	</style>
</head>

<body>

<?php

?>
Neuer Eintrag: <br>
<br>

<form action="db_addRow.php">
	Anrede: <select name="anredetitel">
	<option value="Herr">Herr</option>
	<option value="Frau">Frau</option>
	<option value="Dr">Doktor</option>
	<option value="Prof">Prof.</option>
	</select><br>
	Vorname: <input type="text" name="vorname" value=""><br>
	Nachname: <input type="text" name="nachname" value=""><br>
	Geburtsdatum: <input type="date" name="geburtsdatum" value=""><br>
	Straße: <input type="text" name="strasse" value=""><br>
	PLZ: <input type="number" name="plz" value=""><br>
	Ort: <input type="text" name="ort" value=""><br>
	Telefonnummer: <input type="number" name="telefonnummer" value=""><br>
	E-mail: <input type="text" name="email" value=""><br>
	
	<input type="submit" value="Submit"><br>
</form>

<br><br><br>
Eintrag Löschen: <br>
<br>
<form action="db_deleteRow.php">
	ID: <input type="number" name="id" value=""><br>
	<input type="submit" value="Submit"><br>
</form>

<br><br><br>
Eintrag Bearbeiten: <br>
<br>
<form action="db_updateRow.php">
	ID: <input type="number" name="id" value=""><br>
	Spalte: <select name="column">
	<option value="Anrede">Anrede</option>
	<option value="Vorname">Vorname</option>
	<option value="Nachname">Nachname</option>
	<option value="Geburtsdatum">Geburtsdatum</option>
	<option value="Strasse">Straße</option>
	<option value="PLZ">PLZ</option>
	<option value="Ort">Ort</option>
	<option value="Tummer">Telefonnummer</option>
	<option value="Email">E-Mail</option>
	</select><br>
	Neuer Wert: <input type="text" name="newvalue" value=""><br>
	<input type="submit" value="Submit"><br>
</form>

<?php

include "db_connector.php";

$sql = "SELECT * FROM adressdaten";
$result = mysqli_query($link, $sql);
//echo "Affected rows: " . mysqli_affected_rows($link) . "<br>" ;

?>

<table align="center">
	<tr>
		<td colspan="10"><h1>Adressdaten Datenbank</h1></td>
	</tr>
	<tr>
		<th>ID</th>
		<th>Anrede</th>
		<th>Vorname</th>
		<th>Nachname</th>
		<th>Geburtsdatum</th>
		<th>Straße</th>
		<th>PLZ</th>
		<th>Ort</th>
		<th>Telefonnummer</th>
		<th>Email</th>
	</tr>
	<?php while($rows = mysqli_fetch_assoc($result)){ ?>
	<tr>
		<td><?php echo $rows["ID"] ?></td>
		<td><?php echo $rows["Anrede"] ?></td>
		<td><?php echo $rows["Vorname"] ?></td>
		<td><?php echo $rows["Nachname"] ?></td>
		<td><?php echo $rows["Geburtsdatum"] ?></td>
		<td><?php echo $rows["Strasse"] ?></td>
		<td><?php echo $rows["PLZ"] ?></td>
		<td><?php echo $rows["Ort"] ?></td>
		<td><?php echo $rows["Telefonnummer"] ?></td>
		<td><?php echo $rows["E-Mailadresse"] ?></td>
	</tr>
	<?php } 
	
	mysqli_close($link);?>
	
	
</body>
</html>